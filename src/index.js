import React from 'react';
import ReactDOM from 'react-dom';
import Game from './components/Game';
import {Container, Divider, Segment} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
import './index.css';

<div id="root"></div>

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
