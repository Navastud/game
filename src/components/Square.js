import React from 'react';
import {Grid} from 'semantic-ui-react';

export default class Square extends React.Component {
  render(){
    return (
      <button className="square" onClick={this.props.onClick}>
        {this.props.value}
      </button>
    );
  }
}
