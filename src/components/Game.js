import React from 'react';
import Board from './Board';
import {Segment, Divider, Message, Icon} from 'semantic-ui-react';

export default class Game extends React.Component {
  render(){
    return(
      <Segment>
        <h4>Tic Tac To</h4>
        <Divider></Divider>
        <div className="game">
          <div className="game-board">
            <Board />
          </div>
          <div className="game-info">
            <h5><Icon name="info circle" /> Game Info</h5>
            <p></p>
          </div>
        </div>
      </Segment>
    );
  }
}
